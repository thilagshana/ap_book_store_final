import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import java.awt.Color;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.JPasswordField;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class Login extends JFrame {
	static Login frame;
	private JPanel contentPane;
	private JTextField textField;
	private JPasswordField passwordField;
	static String userName;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					frame = new Login();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Login() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBackground(new Color(216, 191, 216));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JLabel lblNewLabel = new JLabel(" User name  :");
		lblNewLabel.setBounds(36, 37, 116, 20);
		contentPane.add(lblNewLabel);

		JLabel lblPassword = new JLabel("Password :");
		lblPassword.setBounds(36, 102, 102, 14);
		contentPane.add(lblPassword);

		textField = new JTextField();
		textField.setForeground(new Color(220, 20, 60));
		textField.setBackground(new Color(253, 245, 230));
		textField.setBounds(36, 58, 301, 20);
		contentPane.add(textField);
		textField.setColumns(10);

		JButton btnLogin = new JButton("Login");
		btnLogin.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				String name = textField.getText();
				String password = String.valueOf(passwordField.getPassword());
				// System.out.println(name+" "+password);
				userName = name;
				if (UserData.validate(name, password)) {
					UserSearchForm.main(new String[] {});
					frame.dispose();
				} else {
					JOptionPane.showMessageDialog(Login.this, "Sorry, Username or Password Error", "Login Error!",
							JOptionPane.ERROR_MESSAGE);
					textField.setText("");
					passwordField.setText("");
				}
			}
		});
		btnLogin.setForeground(new Color(220, 20, 60));
		btnLogin.setBounds(142, 166, 89, 23);
		contentPane.add(btnLogin);

		passwordField = new JPasswordField();
		passwordField.setBackground(new Color(250, 240, 230));
		passwordField.setToolTipText("");
		passwordField.setForeground(new Color(220, 20, 60));
		passwordField.setBounds(36, 128, 300, 20);
		contentPane.add(passwordField);
	}
}
